package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

public class Group implements Iterable<ClientHandler>{
    private String name;
    private ClientHandler admin;
    private List<ClientHandler> members = new ArrayList<>();
    private List<ClientHandler> banList = new ArrayList<>();

    public Group(String name, ClientHandler admin){
        this.name = name;
        this.admin = admin;
        this.members.add(admin);
    }

    public String getName(){
        return name;
    }

    public ClientHandler getAdmin(){
        return admin;
    }

    public Iterator<ClientHandler> iterator(){
        return members.iterator();
    }

    public ClientHandler getMember(String name){
        for(ClientHandler handler: members){
            if(handler.getName().equals(name)){
                return handler;
            }
        }
        return null;
    }

    public void addMember(ClientHandler handler){
        members.add(handler);
    }

    public boolean isMember(ClientHandler handler){
        return members.contains(handler);
    }

    public void removeMember(ClientHandler handler){
        members.remove(handler);
        if(admin == handler){
            admin = null;
            if(members.size() > 0){
                admin = members.get(0);
            }
        }
    }

    public boolean isBanned(ClientHandler user){
        return banList.contains(user);
    }

    public void banUser(ClientHandler user){
        if(!isBanned(user)){
            banList.add(user);
        }
    }

    public void acceptUser(ClientHandler user){
        banList.remove(user);
    }

    public int getMemberCount(){
        return members.size();
    }
}
