package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

public abstract class Command{
    private String name;
    private Context context;

    public Command(String name){
        this.name = name;
    }

    public final void setContext(Context context){
        this.context = context;
    }

    public final Context getContext(){
        return context;
    }

    public abstract String getCommandFormat();

    public abstract int getArgumentsCount();

    public final String getName(){
        return name;
    }

    public abstract void perform(ClientHandler handler, String... args) throws IOException;

    protected static String convertToString(List<String> list){
        Collections.sort(list);

        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for(String value: list){
            if(first){
                first = false;
            }
            else{
                sb.append(", ");
            }
            sb.append(value);
        }
        return new String(sb);
    }
}
