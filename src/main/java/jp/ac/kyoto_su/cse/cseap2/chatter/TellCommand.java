package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.util.ArrayList;
import java.util.List;
import java.io.IOException;

public class TellCommand extends Command{
    public TellCommand(){
        super("tell");
    }

    @Override
    public int getArgumentsCount(){
        return 2;
    }

    @Override
    public String getCommandFormat(){
        return "tell <to> <message>";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();

        List<ClientHandler> targets = new ArrayList<>();
        String to = args[0];
        String targetName;
        if(context.getClient(to) != null){
            ClientHandler ch = context.getClient(to);
            if(!ch.isReject(handler.getName())){
                targets.add(ch);
                targetName = ch.getName();
            }
            else{
                targetName = null;
            }
        }
        else{
            Group group = context.getGroup(to);
            if(group == null){
                handler.send(to + ": unknown user/group");
                return;
            }
            else if(group.isBanned(handler)){
                handler.send(handler.getName() + ": banned posting to group (" + to + ")");
                return;
            }
            else{
                targetName = "(" + group.getName() + ")";
                for(ClientHandler ch: group){
                    targets.add(ch);
                }
            }
        }

        String sendMessage = String.format(
            "[%s->%s] %s", handler.getName(), targetName, args[1]
        );

        List<String> receivers = new ArrayList<>();
        for(ClientHandler ch: targets){
            if(ch != handler && !ch.isReject(handler.getName())){
                receivers.add(ch.getName());
                ch.send(sendMessage);
            }
        }
        String response = convertToString(receivers);
        if(receivers.size() == 0){
            response = "no one receive message";
        }
        handler.send(response);
    }
}
