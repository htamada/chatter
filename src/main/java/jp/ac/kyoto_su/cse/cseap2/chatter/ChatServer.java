package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 *
 * @author Haruaki Tamada
 */
public class ChatServer{
    public static final int DEFAULT_PORT = 18080;

    private int port = DEFAULT_PORT;
    private int numbers = 0;
    private Context context = new Context();
    private boolean runningFlag = true;

    public ChatServer(String[] args){
        if(args.length == 1){
            setPort(Integer.parseInt(args[0]));
        }
    }

    public ChatServer(){
        this(new String[0]);
    }

    public void start(){
        Thread thread = new Thread(){
            public void run(){
                startServer();
            }
        };
        thread.start();
    }

    public void stop(){
        runningFlag = false;
        try(Socket socket = new Socket("localhost", port)){
            socket.getOutputStream().write(new byte[] { 'b', 'y', 'e', '\r', '\n', });
        } catch(IOException e){
        }
    }

    private void startServer(){
        System.out.println("start server on port " + getPort());
        try(ServerSocket server = new ServerSocket(getPort())){
            while(runningFlag){
                Socket socket = server.accept();
                acceptClient(socket);
            }
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public void setPort(int port){
        if(port < 0){
            throw new IllegalArgumentException("invalid port number: " + port);
        }
        this.port = port;
    }

    public int getPort(){
        return port;
    }

    private void acceptClient(Socket socket) throws IOException{
        numbers++;
        ClientHandler handler = new ClientHandler("undefined" + numbers, socket, context);
        context.register(handler);

        handler.start();
    }

    public static void main(String[] args){
        ChatServer server = new ChatServer(args);
        server.start();
    }
}
