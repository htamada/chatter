package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GroupsCommand extends Command{
    public GroupsCommand(){
        super("groups");
    }

    @Override
    public int getArgumentsCount(){
        return 0;
    }

    @Override
    public String getCommandFormat(){
        return "groups";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();
        List<String> groups = new ArrayList<>();

        for(Iterator<Group> i = context.groups(); i.hasNext(); ){
            Group group = i.next();
            groups.add(group.getName());
        }

        handler.send(convertToString(groups));
    }
}
