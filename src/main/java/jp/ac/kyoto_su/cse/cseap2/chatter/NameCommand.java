package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;

public class NameCommand extends Command{
    public NameCommand(){
        super("name");
    }

    @Override
    public int getArgumentsCount(){
        return 1;
    }

    @Override
    public String getCommandFormat(){
        return "name <new name>";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        String newName = args[0];
        Context context = getContext();

        ClientHandler user = context.getClient(newName);
        if(user != null){
            handler.send(newName + ": user exists.");
            return;
        }
        Group group = context.getGroup(newName);
        if(group != null){
            handler.send(newName + ": group exists.");
            return;
        }

        context.remove(handler);
        handler.setName(newName);
        context.register(handler);

        handler.send("change name to " + newName);
    }
}
