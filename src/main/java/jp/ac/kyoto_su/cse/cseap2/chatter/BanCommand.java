package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;

/**
 * 
 *
 *
 * @author Haruaki Tamada
 */
public class BanCommand extends Command{
    public BanCommand(){
        super("ban");
    }

    @Override
    public int getArgumentsCount(){
        return 2;
    }

    @Override
    public String getCommandFormat(){
        return "ban <group> <user>";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();

        Group group = context.getGroup(args[0]);
        if(group == null){
            handler.send(args[0] + ": unknown group");
            return;
        }

        ClientHandler user = group.getMember(args[1]);
        if(user == null){
            handler.send(args[1] + ": no user in group " + args[0]);
            return;
        }
        if(group.getAdmin() != handler){
            handler.send(handler.getName() + ": not admin of group " + args[0]);
            return;
        }

        String message;
        if(group.isBanned(user)){
            group.acceptUser(user);
            message = user.getName() + ": remove from ban list";
        }
        else{
            group.banUser(user);
            message = user.getName() + ": banned posting to group " + group.getName();
        }
        handler.send(message);
    }
}
