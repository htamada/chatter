package jp.ac.kyoto_su.cse.cseap2.chatter;

public class AlreadyExistsException extends RuntimeException{
	private static final long serialVersionUID = 457315612201183220L;

    public AlreadyExistsException(String message){
        super(message);
    }
}
