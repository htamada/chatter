package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class MembersCommand extends Command{
    public MembersCommand(){
        super("members");
    }

    @Override
    public int getArgumentsCount(){
        return 1;
    }

    @Override
    public String getCommandFormat(){
        return "members <groups>";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();
        List<String> list = new LinkedList<>();

        Group group = context.getGroup(args[0]);
        if(group == null){
            handler.send(args[1] + ": unknown group");
        }
        ClientHandler admin = group.getAdmin();
        for(ClientHandler ch: group){
            if(admin == ch){
                list.add(ch.getName() + "(*)");
            }
            else{
                list.add(ch.getName());
            }
        }

        handler.send(convertToString(list));
    }
}
