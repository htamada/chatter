package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class ClientHandler{
    private String name;
    private Socket socket;
    private PrintWriter out;
    private boolean runningFlag = true;
    private Context context;
    private List<ClientHandler> rejects = new ArrayList<>();

    public ClientHandler(String name, Socket socket, Context context){
        this.socket = socket;
        this.context = context;
        this.setName(name);
    }

    public void rejectUser(ClientHandler handler){
        rejects.add(handler);
    }

    public void acceptUser(ClientHandler handler){
        rejects.remove(handler);
    }

    public boolean isReject(String name){
        for(ClientHandler ch: rejects){
            if(ch.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    public String getName(){
        return name;
    }

    public void setName(String newName){
        this.name = newName;
    }

    public Context getContext(){
        return context;
    }

    public void start(){
        Thread thread = new Thread(){
            public void run(){
                try{
                    startReading();
                } catch(IOException e){
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public synchronized void send(String message) throws IOException{
        out.write(message);
        out.write("\r\n");
        out.flush();
    }

    public void close(){
        runningFlag = false;
    }

    private void startReading() throws IOException{
        try(BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()))){
            this.out = out;

            while(runningFlag){
                String line = in.readLine();
                if(line != null && !line.equals("")){
                    handleCommands(line.trim());
                }
            }
        }
    }

    private void handleCommands(String line) throws IOException{
        StringTokenizer st = new StringTokenizer(line, " \t", true);
        String commandString = findCommand(st);

        Command command = context.getCommand(commandString);
        if(command == null){
            send(commandString + ": unknown command");
        }
        else{
            List<String> arguments = new ArrayList<>();
            parseArguments(command, st, arguments);

            String[] args = arguments.toArray(new String[arguments.size()]);
            if(args.length < command.getArgumentsCount()){
                send("command format error: " + command.getCommandFormat());
                return;
            }

            command.perform(this, arguments.toArray(new String[arguments.size()]));
        }
    }

    private String findCommand(StringTokenizer st){
        String command = null;

        do{
            String next = st.nextToken();
            if(!next.equals("")){
                command = next;
            }
        } while(command == null);

        return command;
    }

    private void parseArguments(Command command, StringTokenizer st, List<String> list){
        StringBuilder sb = new StringBuilder();
        while(st.hasMoreTokens()){
            String token = st.nextToken();
            if(command.getArgumentsCount() == (list.size() + 1)){
                sb.append(token);
            }
            else if(!token.trim().equals("")){
                list.add(token);
            }
        }
        String lastToken = new String(sb).trim();
        if(!lastToken.equals("")){
            list.add(lastToken);
        }
    }
}
