package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ServiceLoader;

public class HelpCommand extends Command{
    private String allCommands = null;

    public HelpCommand(){
        super("help");
    }

    @Override
    public int getArgumentsCount(){
        return 0;
    }

    @Override
    public String getCommandFormat(){
        return "help [command]";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        if(args.length == 0){
            if(allCommands == null){
                allCommands = getCommandList();
            }
            handler.send(allCommands);
        }
        else{
            Context context = handler.getContext();
            Command command = context.getCommand(args[0]);
            if(command == null){
                handler.send(args[0] + ": command not found");
            }
            else{
                handler.send(command.getCommandFormat());
            }
        }
    }

    private String getCommandList(){
        List<String> list = new ArrayList<>();

        ServiceLoader<Command> loader = ServiceLoader.load(Command.class);
        for(Command command: loader){
            list.add(command.getName());
        }

        return convertToString(list);
    }
}
