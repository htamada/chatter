package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;

public class WhoamiCommand extends Command{
    public WhoamiCommand(){
        super("whoami");
    }

    @Override
    public int getArgumentsCount(){
        return 0;
    }

    @Override
    public String getCommandFormat(){
        return "whoami";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        handler.send(handler.getName());
    }
}
