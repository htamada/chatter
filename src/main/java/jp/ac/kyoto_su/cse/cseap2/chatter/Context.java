package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

public class Context implements Iterable<ClientHandler>{
    private Map<String, ClientHandler> clients = new HashMap<>();
    private Map<String, Command> commandMap = new HashMap<>();
    private Map<String, Group> groups = new HashMap<>();

    public Context(){
        registerCommands();
    }

    public void addGroup(Group group){
        if(getGroup(group.getName()) != null){
            throw new AlreadyExistsException(group.getName() + ": group already exists");
        }
        groups.put(group.getName(), group);
    }

    public Group getGroup(String name){
        Group group = groups.get(name);
        return group;
    }

    public void removeGroup(String name){
        groups.remove(name);
    }

    public Iterator<Group> groups(){
        return groups.values().iterator();
    }

    public Iterator<ClientHandler> iterator(){
        return clients.values().iterator();
    }

    public ClientHandler getClient(String name){
        return clients.get(name);
    }

    public void register(ClientHandler handler){
        if(clients.get(handler.getName()) != null){
            throw new AlreadyExistsException(handler.getName() + ": user already exists");
        }
        clients.put(handler.getName(), handler);
    }

    public void remove(ClientHandler handler){
        clients.remove(handler.getName());

        removeFromGroup(handler);
    }

    public Command getCommand(String command){
        command = command.toLowerCase();
        return commandMap.get(command);
    }

    private void registerCommands(){
        ServiceLoader<Command> loader = ServiceLoader.load(Command.class);
        for(Command command: loader){
            command.setContext(this);

            commandMap.put(command.getName().toLowerCase(), command);
        }
    }

    private void removeFromGroup(ClientHandler handler){
        List<Group> groupList = new ArrayList<>(groups.values());

        for(Group group: groupList){
            if(group != null && group.isMember(handler)){
                group.removeMember(handler);
                if(group.getMemberCount() == 0){
                    removeGroup(group.getName());
                }
            }
        }
    }
}
