package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;

public class LeaveCommand extends Command{
    public LeaveCommand(){
        super("leave");
    }

    @Override
    public int getArgumentsCount(){
        return 1;
    }

    @Override
    public String getCommandFormat(){
        return "leave <group name>";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();

        String groupName = args[0];
        Group group = context.getGroup(groupName);

        if(group == null){
            handler.send(groupName + ": unknown group");
        }
        else if(group.isMember(handler)){
            group.removeMember(handler);
            String message = handler.getName() + ": leave from group " + group.getName();
            if(group.getMemberCount() == 0){
                context.removeGroup(group.getName());
                message = group.getName() + ": destroy group";
            }
            handler.send(message);
        }
        else{
            handler.send(handler.getName() + ": not member of group " + group.getName());
        }
    }
}
