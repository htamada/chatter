package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class UsersCommand extends Command{
    public UsersCommand(){
        super("users");
    }

    @Override
    public int getArgumentsCount(){
        return 0;
    }

    @Override
    public String getCommandFormat(){
        return "users";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();
        List<String> list = new LinkedList<>();

        for(ClientHandler ch: context){
            list.add(ch.getName());
        }

        handler.send(convertToString(list));
    }
}
