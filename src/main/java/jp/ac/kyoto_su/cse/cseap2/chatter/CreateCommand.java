package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;

public class CreateCommand extends Command{
    public CreateCommand(){
        super("create");
    }

    @Override
    public int getArgumentsCount(){
        return 1;
    }

    @Override
    public String getCommandFormat(){
        return "create <group name>";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();

        String groupName = args[0];
        if(context.getGroup(groupName) != null){
            handler.send(groupName + ": group exists");
            return;
        }
        else if(context.getClient(groupName) != null){
            handler.send(groupName + ": user exists");
            return;
        }
        Group group = new Group(groupName, handler);
        context.addGroup(group);

        handler.send(groupName + ": create group");
    }
}
