package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class RejectCommand extends Command{
    public RejectCommand(){
        super("reject");
    }

    @Override
    public int getArgumentsCount(){
        return 0;
    }

    @Override
    public String getCommandFormat(){
        return "reject [user]";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();

        if(args.length == 0 || args[0].equals("")){
            sendRejectList(handler, context);
        }
        else{
            ClientHandler user = context.getClient(args[0]);
            if(user == null){
                handler.send(args[0] + ": unknown user");
                return;
            }
            if(handler.isReject(user.getName())){
                handler.acceptUser(user);
                handler.send(user.getName() + ": accept");
            }
            else{
                handler.rejectUser(user);
                handler.send(user.getName() + ": reject");
            }
        }
    }

    private void sendRejectList(ClientHandler handler, Context context) throws IOException{
        List<String> names = new LinkedList<>();

        for(ClientHandler ch: context){
            if(handler.isReject(ch.getName())){
                names.add(ch.getName());
            }
        }
        Collections.sort(names);

        handler.send(convertToString(names));
    }
}
