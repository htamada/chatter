package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PostCommand extends Command{
    public PostCommand(){
        super("post");
    }

    @Override
    public int getArgumentsCount(){
        return 1;
    }

    @Override
    public String getCommandFormat(){
        return "post <message>";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();

        String message = String.format("[%s] %s", handler.getName(), args[0]);

        List<String> members = new ArrayList<>();
        for(ClientHandler ch: context){
            if(ch != handler && !ch.isReject(handler.getName())){
                members.add(ch.getName());
                ch.send(message);
            }
        }

        String response = convertToString(members);
        if(members.size() == 0){
            response = "no one receive message";
        }
        handler.send(response);
    }
}
