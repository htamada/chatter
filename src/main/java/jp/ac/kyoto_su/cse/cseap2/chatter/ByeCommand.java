package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;

public class ByeCommand extends Command{
    public ByeCommand(){
        super("bye");
    }

    @Override
    public int getArgumentsCount(){
        return 0;
    }

    @Override
    public String getCommandFormat(){
        return "bye";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();
        context.remove(handler);

        handler.send(handler.getName() + ": bye");
        handler.close();
    }
}
