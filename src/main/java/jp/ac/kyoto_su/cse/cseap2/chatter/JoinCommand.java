package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;

public class JoinCommand extends Command{
    public JoinCommand(){
        super("join");
    }

    @Override
    public int getArgumentsCount(){
        return 1;
    }

    @Override
    public String getCommandFormat(){
        return "join <group name>";
    }

    @Override
    public void perform(ClientHandler handler, String... args) throws IOException{
        Context context = getContext();

        String groupName = args[0];
        Group group = context.getGroup(groupName);

        String message;
        if(group == null){
            message = groupName + ": unknown group";
        }
        else{
            if(!group.isMember(handler)){
                group.addMember(handler);
                message = handler.getName() + ": join group " + group.getName();
            }
            else{
                message = String.format("%s: already member in group %s", handler.getName(), groupName);
            }
        }
        handler.send(message);
    }
}
