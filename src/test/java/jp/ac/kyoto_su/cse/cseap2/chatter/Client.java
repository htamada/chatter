package jp.ac.kyoto_su.cse.cseap2.chatter;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class Client{
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private List<String> messages = new LinkedList<>();

    public Client() throws IOException{
        this("localhost", ChatServer.DEFAULT_PORT);
    }

    public Client(String host) throws IOException{
        this(host, ChatServer.DEFAULT_PORT);
    }

    public Client(String host, int port) throws IOException{
        socket = new Socket(host, port);
        in  = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));

        Thread thread = new Thread(){
            public void run(){
                while(true){
                    try{
                        String line = in.readLine();
                        if(line == null){
                            break;
                        }
                        messages.add(line.trim());
                    } catch(IOException e){
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    public synchronized String getMessage(){
        if(messages.size() == 0){
            return null;
        }
        String message = messages.get(0);
        messages.remove(0);

        return message;
    }

    public boolean isArriveMessage(){
        return messages.size() > 0;
    }

    public void clear(){
        messages.clear();
    }

    public synchronized void send(String command) throws IOException{
        int size = messages.size();
        out.write(command);
        out.write("\r\n");
        out.flush();

        while(size == messages.size()){
            try{
                Thread.sleep(10);
            } catch(InterruptedException e){
            }
        }
    }
}
