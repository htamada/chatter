package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.is;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class GroupJoinLeaveCommandTest{

    @Test
    public void testBasic() throws IOException{
        Client client1 = new Client();
        Client client2 = new Client();
        Client client3 = new Client();

        client1.send("name c1");
        client2.send("name c2");
        client3.send("name c3");
        client1.clear();
        client2.clear();
        client3.clear();

        client1.send("create g1");
        Assert.assertThat(client1.getMessage(), is("g1: create group"));
        client2.send("join g1");
        Assert.assertThat(client2.getMessage(), is("c2: join group g1"));

        client1.send("tell g1 message to g1 from c1");
        Assert.assertThat(client1.getMessage(), is("c2"));
        Assert.assertThat(client2.getMessage(), is("[c1->(g1)] message to g1 from c1"));

        Assert.assertThat(client1.isArriveMessage(), is(false));
        Assert.assertThat(client3.isArriveMessage(), is(false));

        client1.send("bye");
        client2.send("bye");
        client3.send("bye");
    }

    @Test
    public void testLeaveAndChangeAdmin() throws IOException{
        Client client1 = new Client();
        Client client2 = new Client();
        Client client3 = new Client();

        client1.send("name c1");
        client2.send("name c2");
        client3.send("name c3");
        client1.clear();
        client2.clear();
        client3.clear();

        client1.send("create g1");
        Assert.assertThat(client1.getMessage(), is("g1: create group"));

        client2.send("join g1");
        Assert.assertThat(client2.getMessage(), is("c2: join group g1"));

        client3.send("members g1");
        Assert.assertThat(client3.getMessage(), is("c1(*), c2"));

        client1.send("bye");

        client3.send("members g1");
        Assert.assertThat(client3.getMessage(), is("c2(*)"));

        client2.send("bye");
        client3.send("bye");
    }

}
