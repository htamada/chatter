package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Assert;
import org.junit.Test;

public class PostCommandTest{

    @Test
    public void testInvalidCommandFormat() throws Exception{
        Client client = new Client();

        client.send("post");
        Assert.assertThat(client.getMessage(), is("command format error: post <message>"));

        client.send("bye");
    }

    @Test
    public void testBasic() throws Exception{
        Client client1 = new Client();
        Client client2 = new Client();
        Client client3 = new Client();

        client1.send("name c1");
        client2.send("name c2");
        client3.send("name c3");
        client1.clear();
        client2.clear();
        client3.clear();

        client1.send("post message1 to c2 and c3");
        client2.send("post message2 to c1 and c3");
        client3.send("post message3 to c1 and c2");

        Assert.assertThat(client1.getMessage(), is("c2, c3"));
        Assert.assertThat(client1.getMessage(), is("[c2] message2 to c1 and c3"));
        Assert.assertThat(client1.getMessage(), is("[c3] message3 to c1 and c2"));
        Assert.assertThat(client1.getMessage(), nullValue());

        Assert.assertThat(client2.getMessage(), is("[c1] message1 to c2 and c3"));
        Assert.assertThat(client2.getMessage(), is("c1, c3"));
        Assert.assertThat(client2.getMessage(), is("[c3] message3 to c1 and c2"));
        Assert.assertThat(client2.getMessage(), nullValue());

        Assert.assertThat(client3.getMessage(), is("[c1] message1 to c2 and c3"));
        Assert.assertThat(client3.getMessage(), is("[c2] message2 to c1 and c3"));
        Assert.assertThat(client3.getMessage(), is("c1, c2"));
        Assert.assertThat(client3.getMessage(), nullValue());

        client1.send("bye");
        client2.send("bye");
        client3.send("bye");
    }
}
