package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.is;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class RejectCommandTest{

    @Test
    public void testRejects() throws IOException{
        Client client = new Client();
        client.send("reject");

        Assert.assertThat(client.getMessage(), is(""));
        client.send("bye");
    }

    @Test
    public void testUnknownUserReject() throws IOException{
        Client client = new Client();
        client.send("reject unknown_user");

        Assert.assertThat(client.getMessage(), is("unknown_user: unknown user"));
        client.send("bye");
    }

    @Test
    public void testCommandFormatError() throws IOException{
        Client client1 = new Client();
        Client client2 = new Client();
        Client client3 = new Client();

        client1.send("name hoge1");
        client2.send("name hoge2");
        client3.send("name hoge3");
        client1.clear();
        client2.clear();
        client3.clear();

        client1.send("reject hoge2");
        client2.send("post this message does not reach to hoge1");

        // user names are sorted by its name.
        Assert.assertThat(client1.getMessage(), is("hoge2: reject"));
        Assert.assertThat(client1.isArriveMessage(), is(false));
        Assert.assertThat(client3.getMessage(), is("[hoge2] this message does not reach to hoge1"));

        client1.send("bye");
        client2.send("bye");
        client3.send("bye");
    }

    @Test
    public void testRejectAndAcceptUser() throws Exception{
        Client client1 = new Client();
        Client client2 = new Client();

        client1.send("name hoge1");
        client2.send("name hoge2");
        client1.clear();
        client2.clear();

        client1.send("reject hoge2");
        client1.send("reject");
        client1.send("reject hoge2");
        client1.send("reject");
        Assert.assertThat(client1.getMessage(), is("hoge2: reject"));
        Assert.assertThat(client1.getMessage(), is("hoge2"));
        Assert.assertThat(client1.getMessage(), is("hoge2: accept"));
        Assert.assertThat(client1.getMessage(), is(""));

        client1.send("bye");
        client2.send("bye");
    }

    @Test
    public void testShowRejectList() throws Exception{
        Client client1 = new Client();
        Client client2 = new Client();
        Client client3 = new Client();
        Client client4 = new Client();

        client1.send("name hoge1");
        client2.send("name hoge2");
        client3.send("name hoge3");
        client4.send("name hoge4");
        client1.clear();
        client2.clear();
        client3.clear();
        client4.clear();

        client1.send("reject hoge4");
        client1.send("reject hoge2");
        client1.clear();
        client1.send("reject");
        Assert.assertThat(client1.getMessage(), is("hoge2, hoge4"));

        client1.send("bye");
        client2.send("bye");
        client3.send("bye");
        client4.send("bye");
    }
}
