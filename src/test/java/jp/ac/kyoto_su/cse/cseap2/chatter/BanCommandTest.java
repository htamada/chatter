package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Assert;
import org.junit.Test;

public class BanCommandTest{
    @Test
    public void testInvalidCommandFormat() throws Exception{
        Client client = new Client();
        client.send("ban g");

        Assert.assertThat(client.getMessage(), is("command format error: ban <group> <user>"));

        client.send("bye");
    }

    @Test
    public void testNotAdmin() throws Exception{
        Client client1 = new Client();
        Client client2 = new Client();

        client1.send("name c1");
        client2.send("name c2");
        client1.clear();
        client2.clear();

        client1.send("create g1");
        client2.send("ban g1 c1");

        Assert.assertThat(client1.getMessage(), is("g1: create group"));
        Assert.assertThat(client2.getMessage(), is("c2: not admin of group g1"));

        client1.send("ban g1 c2");
        Assert.assertThat(client1.getMessage(), is("c2: no user in group g1"));

        client1.send("bye");
        client2.send("bye");
    }

    @Test
    public void testUnknownUser() throws Exception{
        Client client = new Client();

        client.send("name c31");
        client.send("create g31");
        client.clear();
        client.send("ban g31 c32");
        Assert.assertThat(client.getMessage(), is("c32: no user in group g31"));

        client.send("ban g32 c32");
        Assert.assertThat(client.getMessage(), is("g32: unknown group"));

        client.send("bye");
    }

    @Test
    public void testBasic() throws Exception{
        Client client1 = new Client();
        Client client2 = new Client();
        Client client3 = new Client();

        client1.send("name c1");
        client2.send("name c2");
        client3.send("name c3");
        client1.clear();
        client2.clear();
        client3.clear();

        client1.send("create g1");
        client2.send("join g1");
        client3.send("join g1");

        client1.send("ban g1 c3");

        client1.clear();
        client2.clear();
        client3.clear();

        client1.send("tell g1 message1");
        Assert.assertThat(client1.getMessage(), is("c2, c3"));
        Assert.assertThat(client2.getMessage(), is("[c1->(g1)] message1"));
        Assert.assertThat(client3.getMessage(), is("[c1->(g1)] message1"));

        client2.send("tell g1 message2");
        Assert.assertThat(client1.getMessage(), is("[c2->(g1)] message2"));
        Assert.assertThat(client2.getMessage(), is("c1, c3"));
        Assert.assertThat(client3.getMessage(), is("[c2->(g1)] message2"));

        client3.send("tell g1 message3");
        Assert.assertThat(client1.getMessage(), nullValue());
        Assert.assertThat(client2.getMessage(), nullValue());
        Assert.assertThat(client3.getMessage(), is("c3: banned posting to group (g1)"));

        client1.send("bye");
        client2.send("bye");
        client3.send("bye");
    }
}
