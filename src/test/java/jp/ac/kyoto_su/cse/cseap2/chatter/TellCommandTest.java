package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Assert;
import org.junit.Test;

public class TellCommandTest{

    @Test
    public void testInvalidCommandFormat() throws Exception{
        Client client = new Client();

        client.send("tell");
        Assert.assertThat(
            client.getMessage(),
            is("command format error: tell <user | group> <message>")
        );

        client.send("bye");
    }

    @Test
    public void testBasic() throws Exception{
        Client client1 = new Client();
        Client client2 = new Client();
        Client client3 = new Client();

        client1.send("name c1");
        client2.send("name c2");
        client3.send("name c3");
        client1.clear();
        client2.clear();
        client3.clear();

        client1.send("tell c2 message1 to c2 from c1");
        Assert.assertThat(client1.getMessage(), is("c2"));
        Assert.assertThat(client2.getMessage(), is("[c1->c2] message1 to c2 from c1"));
        Assert.assertThat(client3.getMessage(), nullValue());

        client2.send("tell c3 message2 to c3 from c2");
        Assert.assertThat(client2.getMessage(), is("c3"));
        Assert.assertThat(client3.getMessage(), is("[c2->c3] message2 to c3 from c2"));
        Assert.assertThat(client1.getMessage(), nullValue());

        client3.send("tell c1 message3 to c1 from c3");
        Assert.assertThat(client3.getMessage(), is("c1"));
        Assert.assertThat(client1.getMessage(), is("[c3->c1] message3 to c1 from c3"));
        Assert.assertThat(client2.getMessage(), nullValue());

        client1.send("bye");
        client2.send("bye");
        client3.send("bye");
    }
}
