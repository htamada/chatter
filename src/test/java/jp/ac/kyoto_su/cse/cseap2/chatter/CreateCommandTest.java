package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class CreateCommandTest{

    @Test
    public void testBasic() throws IOException{
        Client client = new Client();

        client.send("create group1");
        client.send("create group2");
        client.send("groups");

        String message1 = client.getMessage();
        String message2 = client.getMessage();
        String message3 = client.getMessage();
        Assert.assertThat(message1, is("group1: create group"));
        Assert.assertThat(message2, is("group2: create group"));
        Assert.assertThat(message3, is("group1, group2"));

        client.send("bye");
    }

    @Test
    public void testError() throws IOException{
        Client client1 = new Client();
        Client client2 = new Client();

        client1.send("name c1");
        client2.send("name c2");
        client1.clear();
        client2.clear();

        client1.send("create g1");
        client2.send("create g1");
        client2.send("create c1");

        String message1 = client1.getMessage();
        String message2 = client2.getMessage();
        String message3 = client2.getMessage();
        Assert.assertThat(message1, is("g1: create group"));
        Assert.assertThat(message2, is("g1: group exists"));
        Assert.assertThat(message3, is("c1: user exists"));

        client1.send("bye");
        client2.send("bye");
    }

    @Test
    public void testMultiUsers() throws IOException{
        Client client1 = new Client();
        Client client2 = new Client();

        client1.send("create group2");
        client2.send("create group1");
        client2.send("groups");

        String message11 = client1.getMessage();
        String message21 = client2.getMessage();
        String message22 = client2.getMessage();

        Assert.assertThat(message11, allOf(notNullValue(), is("group2: create group")));
        Assert.assertThat(message21, allOf(notNullValue(), is("group1: create group")));
        Assert.assertThat(message22, allOf(notNullValue(), is("group1, group2")));

        client1.send("bye");

        client2.send("groups");
        String message23 = client2.getMessage();
        Assert.assertThat(message23, allOf(notNullValue(), is("group1")));

        client2.send("bye");
    }
}
