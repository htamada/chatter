package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.is;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class HelpCommandTest{

    @Test
    public void testBasic() throws IOException{
        Client client = new Client();
        client.send("help");

        Assert.assertThat(
            client.getMessage(),
            is("ban, bye, create, groups, help, join, leave, members, name, post, reject, tell, users, whoami")
        );
        client.send("bye");
    }

    @Test
    public void testHelpMessage() throws IOException{
        Client client = new Client();

        client.send("help bye");
        Assert.assertThat(client.getMessage(), is("bye"));

        client.send("help reject");
        Assert.assertThat(client.getMessage(), is("reject [user]"));

        client.send("help help");
        Assert.assertThat(client.getMessage(), is("help [command]"));

        client.send("help post");
        Assert.assertThat(client.getMessage(), is("post <message>"));

        client.send("bye");
    }

    @Test
    public void testUnknownCommand() throws IOException{
        Client client = new Client();

        client.send("help hoge");
        Assert.assertThat(client.getMessage(), is("hoge: command not found"));
    }
}
