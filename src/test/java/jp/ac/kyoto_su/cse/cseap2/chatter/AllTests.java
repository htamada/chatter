package jp.ac.kyoto_su.cse.cseap2.chatter;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses( {
    ConnectionTest.class,
    NameCommandTest.class,
    UsersCommandTest.class,
    PostCommandTest.class,
    TellCommandTest.class,
    RejectCommandTest.class,
    GroupJoinLeaveCommandTest.class,
    BanCommandTest.class,
    HelpCommandTest.class
})
public class AllTests {
    private static ChatServer server;

    @BeforeClass
    public static void beforeClass() throws Exception {
        server = new ChatServer();
        server.start();
    }

    @AfterClass
    public static void afterClass() {
        server.stop();
    }
}
