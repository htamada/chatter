package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.startsWith;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class NameCommandTest {

    @Test
    public void testDefaultName() throws IOException {
        Client client = new Client();
        client.send("WHOAMI");

        Assert.assertThat(client.getMessage(), startsWith("undefined"));

        client.send("bye");
    }

    @Test
    public void testNameCommand() throws IOException {
        Client client = new Client();
        client.send("whoami");
        client.send("name hogehoge");
        client.send("whoami");

        Assert.assertThat(client.getMessage(), startsWith("undefined"));
        Assert.assertThat(client.getMessage(), is("change name to hogehoge"));
        Assert.assertThat(client.getMessage(), is("hogehoge"));

        client.send("bye");
    }

    @Test
    public void testCommandFormatError() throws IOException {
        Client client = new Client();

        client.send("name");
        Assert.assertThat(client.getMessage(), is("command format error: name <name>"));

        client.send("bye");
    }

    @Test
    public void testDuplicateName() throws IOException {
        Client client1 = new Client();
        Client client2 = new Client();

        client1.send("name hoge1");
        client1.send("whoami");
        client2.send("name hoge1");
        client2.send("whoami");

        Assert.assertThat(client1.getMessage(), is("change name to hoge1"));
        Assert.assertThat(client1.getMessage(), is("hoge1"));

        Assert.assertThat(client2.getMessage(), is("hoge1: already used"));
        Assert.assertThat(client2.getMessage(), startsWith("undefined"));

        client1.send("bye");
        client2.send("bye");
    }
}
