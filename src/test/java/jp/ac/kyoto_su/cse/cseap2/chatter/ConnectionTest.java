package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.endsWith;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;

import org.junit.Assert;
import org.junit.Test;

public class ConnectionTest{

    @Test
    public void testBasic() throws Exception{
        Client client = new Client();
        client.send("bye");
        Assert.assertThat(client.getMessage(), endsWith("bye"));
        Assert.assertThat(client.getMessage(), nullValue());
    }

    @Test
    public void testUnknownCommand() throws Exception{
        Client client = new Client();
        client.send("unknown_command");
        Assert.assertThat(client.getMessage(), is("unknown_command: unknown command"));

        client.send("bye");
    }


    @Test
    public void testUnknownCommand2() throws Exception{
        Client client = new Client();
        client.send("   unknown_command");
        Assert.assertThat(client.getMessage(), is("unknown_command: unknown command"));

        client.send("bye");
    }
}
