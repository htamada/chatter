package jp.ac.kyoto_su.cse.cseap2.chatter;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.startsWith;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

public class UsersCommandTest{

    @Test
    public void testUsers() throws IOException{
        Client client = new Client();
        client.send("users");

        Assert.assertThat(client.getMessage(), startsWith("undefined"));
        client.send("bye");
    }

    @Test
    public void testCommandFormatError() throws IOException{
        Client client1 = new Client();
        Client client2 = new Client();

        client1.send("name hoge1");
        client2.send("name hoge2");
        client1.clear();
        client2.clear();

        client1.send("users");
        client2.send("users");

        Assert.assertThat(client1.getMessage(), allOf(notNullValue(), is("hoge1, hoge2")));
        Assert.assertThat(client2.getMessage(), allOf(notNullValue(), is("hoge1, hoge2")));

        client1.send("bye");
        client2.send("bye");
    }
}
